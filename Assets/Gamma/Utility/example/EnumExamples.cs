﻿using UnityEngine;
using System.Collections;
using System;

public class EnumExamples : MonoBehaviour {

	[FlagsAttribute]
	public enum MySpecialEnum {
		Value1,
		Value2,
		Value3
	}

	// Use this for initialization
	void Start () {
		MySpecialEnum enu = Enum<MySpecialEnum>.Parse("Value1");

		enu = enu | MySpecialEnum.Value2;

		Debug.Log(enu.HasFlag(MySpecialEnum.Value2));
	}
}
