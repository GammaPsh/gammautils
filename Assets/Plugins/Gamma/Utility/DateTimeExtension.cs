using System;

public static class DateTimeExtension {
	
	// Convert Datetime to Unix Timestamp, besst used with utc time ;)
	public static long ToUnixTimeStamp(this DateTime dateTime){
		
		DateTime unixtime = new DateTime(1970,1,1,0,0,0);
		
		TimeSpan timespan = dateTime - unixtime;
		return (long)timespan.TotalSeconds;
	}
	
	public static DateTime FromUnixTimeStamp(this long unixTimestamp){
		
		DateTime unixtime = new DateTime(1970,1,1,0,0,0);
		
		TimeSpan timespan = TimeSpan.FromSeconds(unixTimestamp);
		return unixtime + timespan;
	}
	
	// Convert Datetime to Millenium Timestamp, ios uses this timestamp
	public static long ToMilleniumTimeStamp(this DateTime dateTime){
		
		DateTime unixtime = new DateTime(2000,1,1,0,0,0);
		
		TimeSpan timespan = dateTime - unixtime;
		return (long)timespan.TotalSeconds;
	}
	
	public static DateTime FromMilleniumTimeStamp(this long milleniumTimestamp){
		
		DateTime unixtime = new DateTime(2000,1,1,0,0,0);
		
		TimeSpan timespan = TimeSpan.FromSeconds(milleniumTimestamp);
		return unixtime + timespan;
	}
}
