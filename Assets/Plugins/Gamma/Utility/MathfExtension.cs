using UnityEngine;
using System.Collections;

public static class MathfExtension {

	public static float ClampAngle (float angle, float min, float max){
		
		if(min == 0 && max == 0)
			return angle;
		
		// Bring all values to 0°- 360° space
		angle = angle >= 0 ? angle%360 : angle%360+360;
		min = min >= 0 ? min%360 : min%360+360;
		max = max >= 0 ? max%360 : max%360+360;
		
		//Debug.Log(angle +", "+ min +", "+ max);
		
		if(min <= max){
			
			if(min < angle && angle < max)
				return angle;
			
			float middlevalue = (min+max)/2;
			
			if(middlevalue > 180){
				return (angle < min && angle > middlevalue-180) ? min : max;
			}else
				return (angle > max && angle < middlevalue+180) ? max : min;
		}
		
		//We want to limit a Rotation over 360°/0°
		if(angle <= max){
			return angle;
		}
		
		if(angle >= min){
			return angle;
		}
		
		return angle < (min+max)/2 ? max: min;
	}
}