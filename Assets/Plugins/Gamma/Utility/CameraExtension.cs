﻿using UnityEngine;
using System.Collections;

public static class CameraExtension {
	
	public static float HorizontalFOV(this Camera cam){
		return Mathf.Rad2Deg * 2 * Mathf.Atan(Mathf.Tan(cam.fieldOfView * Mathf.Deg2Rad / 2) * cam.aspect);
	}
}

