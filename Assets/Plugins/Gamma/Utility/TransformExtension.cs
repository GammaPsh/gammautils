﻿using UnityEngine;
using System.Collections;

public static class TransformExtension {

	public static void SetParent(this Transform transform, Transform parent, Vector3 localPosition, Vector3 localScale, Quaternion localRotation){
		transform.SetParent(parent);
		transform.localPosition = localPosition;
		transform.localScale = localScale;
		transform.localRotation = localRotation;
	}
}
