﻿#if !UNITY_WEBPLAYER
using UnityEngine;
using System.Collections;
using System.IO;

public static class DirectoryInfoExtension {

	public static void CopyTo(this DirectoryInfo directoryInfo, string destinationPath)
	{
		if(!Directory.Exists(destinationPath)){
			Directory.CreateDirectory(destinationPath);
		}

		FileInfo[] fileInfos =  directoryInfo.GetFiles();
		foreach(var fInfo in fileInfos){
			fInfo.CopyTo(Path.Combine(destinationPath, fInfo.Name));
		}

		DirectoryInfo[] dirInfos = directoryInfo.GetDirectories();
		foreach(var dInfo in dirInfos){
			dInfo.CopyTo(Path.Combine(destinationPath, dInfo.Name));
		}
	}
}
#endif
