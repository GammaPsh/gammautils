using UnityEngine;
using System.Collections;

namespace Gamma.Units {
	public class UnitConversion {
		
	#region Distance
		/// <summary>
		/// Convert cm to inches
		/// </summary>
		public static float cm_TO_inches(float cm_Value){
			return cm_Value/2.54f;
		}

		/// <summary>
		/// Convert inches to cm
		/// </summary>
		public static float inches_TO_cm(float inch_Value){
			return inch_Value*2.54f;
		}

		/// <summary>
		/// Convert mm to inches
		/// </summary>
		public static float mm_TO_inches(float mm_Value){
			return 	mm_Value/25.4f;
		}

		/// <summary>
		/// Convert inches to mm
		/// </summary>
		public static float inches_TO_mm(float inch_value){
			return 	inch_value*25.4f;
		}

		/// <summary>
		/// Convert meter to yard
		/// </summary>
		public static float m_TO_yd(float meter_Value){
			return meter_Value/0.9144f;
		}

		/// <summary>
		/// Convert yard to meter
		/// </summary>
		public static float yd_TO_m(float yard_Value){
			return yard_Value*0.9144f;
		}
	#endregion

	#region Squared Distance
		/// <summary>
		/// Convert squaremeters to squareyards
		/// </summary>
		public static float m2_TO_yd2(float m2_Value){
			return m2_Value/(0.9144f*0.9144f);
		}

		/// <summary>
		/// Convert squareyard to squaremeter
		/// </summary>
		public static float yd2_TO_m2(float yd2_Value){
			return yd2_Value*(0.9144f*0.9144f);
		}
	#endregion	

	#region Mass
		/// <summary>
		/// Convert gram to ounces
		/// </summary>
		public static float gram_TO_ounce(float gram_Value){
			return gram_Value/28.3495231f;
		}

		/// <summary>
		/// Convert ounces to gram
		/// </summary>
		public static float ounce_TO_gram(float ounce_Value){
			return ounce_Value*28.3495231f;
		}
		
	#endregion
		
	#region specialcases
		/// <summary>
		/// Convert gram per Squaremeter to ounces per Squareyard
		/// </summary>
		public static float g_m2_TO_oz_yd2(float g_m2_Value){
			//1 = 0.029494
			return g_m2_Value/33.905f;
		}

		/// <summary>
		/// Convert ounces per Squareyard to gram per squaremeter
		/// </summary>
		public static float oz_yd2_TO_g_m2(float oz_yd2_Value){
			//1 = 0.029494
			return oz_yd2_Value*33.905f;
		}
	#endregion
	}
}